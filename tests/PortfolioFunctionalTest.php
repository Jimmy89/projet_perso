<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PortfolioFunctionalTest extends WebTestCase
{
    public function testShouldDisplayPortfolio()
    {
        $client = static::createClient();
        //URL de la page testé
        $crawler = $client->request('GET', '/portfolio');

        $this->assertResponseIsSuccessful();
        //Vérification de la présence de la balise '' , puis de son contenu ''
        $this->assertSelectorTextContains('h2', 'Portfolio');
    }

    public function testShouldDisplayOnePortfolio()
    {
        $client = static::createClient();
        //URL de la page testé
        $crawler = $client->request('GET', '/portfolio/categorie-test');

        $this->assertResponseIsSuccessful();
        //Vérification de la présence de la balise '' , puis de son contenu ''
        $this->assertSelectorTextContains('h2', 'categorie test');
    }
}
