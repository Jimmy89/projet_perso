<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ContactFunctionalTest extends WebTestCase
{
    public function testShouldDisplayContact()
    {
        $client = static::createClient();
        //URL de la page testé
        $crawler = $client->request('GET', '/contact');

        $this->assertResponseIsSuccessful();
        //Vérification de la présence de la balise '' , puis de son contenu ''
        $this->assertSelectorTextContains('h2', 'Nous Contacter');
    }
}
