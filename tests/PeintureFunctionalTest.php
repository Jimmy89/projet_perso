<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PeintureFunctionalTest extends WebTestCase
{
    public function testShouldDisplayPeinture()
    {
        $client = static::createClient();
        //URL de la page testé
        $crawler = $client->request('GET', '/realisations');

        $this->assertResponseIsSuccessful();
        //Vérification de la présence de la balise '' , puis de son contenu ''
        $this->assertSelectorTextContains('h2', 'Dernières Réalisations');
    }

    public function testShouldDisplayOnePeinture()
    {
        $client = static::createClient();
        //URL de la page testé
        $crawler = $client->request('GET', '/realisations/peinture-test');

        $this->assertResponseIsSuccessful();
        //Vérification de la présence de la balise '' , puis de son contenu ''
        $this->assertSelectorTextContains('h5', 'Peinture test');
    }
}
