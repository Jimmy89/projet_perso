<?php

namespace App\Tests;

use App\Entity\Contact;
use DateTime;
use PHPUnit\Framework\TestCase;

class ContactUnitTest extends TestCase
{
    public function testContactIsTrue(): void
    {
        $contact = new Contact();
        $datetime = new DateTime();

        $contact->setEmail('user@user.fr')
             ->setNom('nom')
             ->setMessage('instagram')
             ->setCreatedAt($datetime)
             ->setIsSend(true);

        $this->assertTrue($contact->getEmail() === 'user@user.fr');
        $this->assertTrue($contact->getNom() === 'nom');
        $this->assertTrue($contact->getMessage() === 'instagram');
        $this->assertTrue($contact->getCreatedAt() === $datetime);
        $this->assertTrue($contact->getIsSend() === true);

    }

    public function testUserIsFalse(): void
    {
        $contact = new Contact();
        $datetime = new DateTime();

        $contact->setEmail('user@user.fr')
             ->setNom('nom')
             ->setMessage('instagram')
             ->setCreatedAt($datetime)
             ->setIsSend(true);

        $this->assertFalse($contact->getEmail() === 'False');
        $this->assertFalse($contact->getNom() === 'False');
        $this->assertFalse($contact->getMessage() === 'False');
        $this->assertFalse($contact->getCreatedAt() === new DateTime());
        $this->assertFalse($contact->getIsSend() === false);
    }

    public function testUserIsEmpty(): void
    {
        $contact = new Contact();
        
        $this->assertEmpty($contact->getEmail());
        $this->assertEmpty($contact->getNom());
        $this->assertEmpty($contact->getMessage());
        $this->assertEmpty($contact->getCreatedAt());
        $this->assertEmpty($contact->getIsSend());
        $this->assertEmpty($contact->getId());

    }
}
