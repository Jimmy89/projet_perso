<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use DateTime;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    public function testCommentaireIsTrue(): void
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $blogpost = new Blogpost();
        $peinture = new Peinture();

        $commentaire->setAuteur('Auteur')
             ->setEmail('email@test.com')
             ->setContenu('contenu')
             ->setPeinture($peinture)
             ->setBlogpost($blogpost)
             ->setCreatedAt($datetime);

        $this->assertTrue($commentaire->getAuteur() === 'Auteur');
        $this->assertTrue($commentaire->getContenu() === 'contenu');
        $this->assertTrue($commentaire->getEmail() === 'email@test.com');
        $this->assertTrue($commentaire->getCreatedAt() === $datetime);
        $this->assertTrue($commentaire->getBlogpost() === $blogpost);
        $this->assertTrue($commentaire->getPeinture() === $peinture);
    }

    public function testCommentaireIsFalse(): void
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $blogpost = new Blogpost();
        $peinture = new Peinture();

        $commentaire->setAuteur('Auteur')
             ->setEmail('email@test.com')
             ->setContenu('contenu')
             ->setPeinture($peinture)
             ->setBlogpost($blogpost)
             ->setCreatedAt($datetime);

        $this->assertFalse($commentaire->getAuteur() === 'false');
        $this->assertFalse($commentaire->getContenu() === 'false');
        $this->assertFalse($commentaire->getEmail() === 'false@test.com');
        $this->assertFalse($commentaire->getCreatedAt() === new DateTime());
        $this->assertFalse($commentaire->getBlogpost() === new Blogpost());
        $this->assertFalse($commentaire->getPeinture() === new Peinture());
    }

    public function testCommentaireIsEmpty(): void
    {
        $commentaire = new Commentaire();
        
        $this->assertEmpty($commentaire->getAuteur());
        $this->assertEmpty($commentaire->getContenu());
        $this->assertEmpty($commentaire->getEmail());
        $this->assertEmpty($commentaire->getCreatedAt());
        $this->assertEmpty($commentaire->getBlogpost());
        $this->assertEmpty($commentaire->getPeinture());
        $this->assertEmpty($commentaire->getId());
    }
}
