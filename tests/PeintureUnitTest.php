<?php

namespace App\Tests;

use App\Entity\Category;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class PeintureUnitTest extends TestCase
{
    public function testPeintureIsTrue(): void
    {
        $peinture = new Peinture();
        $datetime = new DateTime();
        $category = new Category();
        $user = new User();
        $peinture->setNom('nom')
             ->setLargeur(20.20)
             ->setHauteur(20.20)
             ->setEnVente(true)
             ->setPrix(20.20)
             ->setDateRealisation($datetime)
             ->setCreatedAt($datetime)
             ->setDescription('description')
             ->setPortfolio(true)
             ->setSlug('slug')
             ->setFile('file')
             ->addCategory($category)
             ->setUser($user);

        $this->assertTrue($peinture->getLargeur() == 20.20);
        $this->assertTrue($peinture->getHauteur() == 20.20);
        $this->assertTrue($peinture->getNom() === 'nom');
        $this->assertTrue($peinture->getEnVente() === true);
        $this->assertTrue($peinture->getPrix() == 20.20);
        $this->assertTrue($peinture->getDateRealisation() === $datetime);
        $this->assertTrue($peinture->getCreatedAt() === $datetime);
        $this->assertTrue($peinture->getDescription() === 'description');
        $this->assertTrue($peinture->getPortfolio() === true);
        $this->assertTrue($peinture->getSlug() === 'slug');
        $this->assertTrue($peinture->getFile() === 'file');
        $this->assertContains($category,$peinture->getCategory());
        $this->assertTrue($peinture->getUser() === $user);
    }

    public function testPeintureIsFalse(): void
    {
        $peinture = new Peinture();
        $datetime = new DateTime();
        $category = new Category();
        $user = new User();
        $peinture->setNom('nom')
             ->setLargeur(20.20)
             ->setHauteur(20.20)
             ->setEnVente(true)
             ->setPrix(20.20)
             ->setDateRealisation($datetime)
             ->setCreatedAt($datetime)
             ->setDescription('description')
             ->setPortfolio(true)
             ->setSlug('slug')
             ->setFile('file')
             ->addCategory($category)
             ->setUser($user);

        $this->assertFalse($peinture->getLargeur() == 22.20);
        $this->assertFalse($peinture->getHauteur() == 22.20);
        $this->assertFalse($peinture->getNom() === 'false');
        $this->assertFalse($peinture->getEnVente() === false);
        $this->assertFalse($peinture->getPrix() == 22.20);
        $this->assertFalse($peinture->getDateRealisation() === new DateTime());
        $this->assertFalse($peinture->getCreatedAt() === new DateTime());
        $this->assertFalse($peinture->getDescription() === 'false');
        $this->assertFalse($peinture->getPortfolio() === false);
        $this->assertFalse($peinture->getSlug() === 'false');
        $this->assertFalse($peinture->getFile() === 'false');
        $this->assertNotContains(new Category(), $peinture->getCategory());
        $this->assertFalse($peinture->getUser() === new User());
    }

    public function testPeintureIsEmpty(): void
    {
        $peinture = new Peinture();
        
        $this->assertEmpty($peinture->getLargeur());
        $this->assertEmpty($peinture->getHauteur());
        $this->assertEmpty($peinture->getNom());
        $this->assertEmpty($peinture->getEnVente());
        $this->assertEmpty($peinture->getPrix());
        $this->assertEmpty($peinture->getDateRealisation());
        $this->assertEmpty($peinture->getCreatedAt());
        $this->assertEmpty($peinture->getDescription());
        $this->assertEmpty($peinture->getPortfolio());
        $this->assertEmpty($peinture->getSlug());
        $this->assertEmpty($peinture->getFile());
        $this->assertEmpty($peinture->getCategory());
        $this->assertEmpty($peinture->getUser());
        $this->assertEmpty($peinture->getId());
    }

    public function testAddGetRemoveCommentaire()
     {
        //initialisation du test 
        $peinture = new Peinture();
        $commentaire = new Commentaire();

        //test à vide
        $this->assertEmpty($peinture->getCommentaires());

        //test ajout de commentaire
        $peinture->addCommentaire($commentaire);
        $this->assertContains($commentaire, $peinture->getCommentaires());

        //test suppression de commentaire
        $peinture->removeCommentaire($commentaire);
        $this->assertEmpty($peinture->getCommentaires());
     }
}
