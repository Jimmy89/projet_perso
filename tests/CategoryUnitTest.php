<?php

namespace App\Tests;

use App\Entity\Category;
use App\Entity\Peinture;
use PHPUnit\Framework\TestCase;

class CategoryUnitTest extends TestCase
{
    public function testCategoryIsTrue(): void
    {
        $category = new Category();
        $category->setNom('nom')
             ->setDescription('description')
             ->setSlug('slug');

        $this->assertTrue($category->getNom() === 'nom');
        $this->assertTrue($category->getDescription() === 'description');
        $this->assertTrue($category->getSlug() === 'slug');
    }

    public function testCategoryIsFalse(): void
    {
        $category = new Category();
        $category->setNom('nom')
             ->setDescription('description')
             ->setSlug('slug');

             $this->assertFalse($category->getNom() === 'false');
             $this->assertFalse($category->getDescription() === 'false');
             $this->assertFalse($category->getSlug() === 'false');
    }

    public function testCategoryIsEmpty(): void
    {
        $category = new Category();
        
        $this->assertEmpty($category->getNom());
             $this->assertEmpty($category->getDescription());
             $this->assertEmpty($category->getSlug());
             $this->assertEmpty($category->getId());
    }

    public function testAddGetRemovepeinture()
     {
        //initialisation du test 
        $category = new Category();
        $peinture = new Peinture();

        //test à vide
        $this->assertEmpty($category->getPeintures());

        //test ajout de Peintures
        $category->addPeinture($peinture);
        $this->assertContains($peinture, $category->getPeintures());

        //test suppression de Peintures
        $category->removePeinture($peinture);
        $this->assertEmpty($category->getPeintures());
     }
}
