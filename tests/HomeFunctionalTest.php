<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeFunctionalTest extends WebTestCase
{
    public function testShouldDisplayHomepage()
    {
        $client = static::createClient();
        //URL de la page testé
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        //Vérification de la présence de la balise '' , puis de son contenu ''
        $this->assertSelectorTextContains('h1', 'Hello World');
    }
}
