<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\Peinture;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testUserIsTrue(): void
    {
        $user = new User();
        $user->setEmail('user@user.fr')
             ->setPrenom('prenom')
             ->setNom('nom')
             ->setAPropos('a propos')
             ->setTelephone('0123456789')
             ->setInstagram('instagram')
             ->setPassword('password')
             ->setRoles(['ROLE_TEST']);

        $this->assertTrue($user->getEmail() === 'user@user.fr');
        $this->assertTrue($user->getUsername() === 'user@user.fr');
        $this->assertTrue($user->getUserIdentifier() === 'user@user.fr');
        $this->assertTrue($user->getPrenom() === 'prenom');
        $this->assertTrue($user->getNom() === 'nom');
        $this->assertTrue($user->getAPropos() === 'a propos');
        $this->assertTrue($user->getTelephone() === '0123456789');
        $this->assertTrue($user->getInstagram() === 'instagram');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getRoles() === ['ROLE_TEST', 'ROLE_USER']);
    }

    public function testUserIsFalse(): void
    {
        $user = new User();
        $user->setEmail('user@user.fr')
             ->setPrenom('prenom')
             ->setNom('nom')
             ->setAPropos('a propos')
             ->setTelephone('0123456789')
             ->setInstagram('instagram')
             ->setPassword('password');

        $this->assertFalse($user->getEmail() === 'false@user.fr');
        $this->assertFalse($user->getPrenom() === 'false');
        $this->assertFalse($user->getNom() === 'false');
        $this->assertFalse($user->getAPropos() === 'false');
        $this->assertFalse($user->getTelephone() === 'false');
        $this->assertFalse($user->getInstagram() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
    }

    public function testUserIsEmpty(): void
    {
        $user = new User();
        
        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getAPropos());
        $this->assertEmpty($user->getTelephone());
        $this->assertEmpty($user->getInstagram());
        $this->assertEmpty($user->getId());
    }

    public function testAddGetRemovePeinture()
     {
        //initialisation du test 
        $peinture = new Peinture();
        $user = new User();

        //test à vide
        $this->assertEmpty($user->getPeintures());

        //test ajout de peinture
        $user->addPeinture($peinture);
        $this->assertContains($peinture, $user->getPeintures());

        //test suppression de peinture
        $user->removePeinture($peinture);
        $this->assertEmpty($user->getPeintures());
     }

     public function testAddGetRemoveBlogpost()
     {
        //initialisation du test 
        $blogpost = new Blogpost();
        $user = new User();

        //test à vide
        $this->assertEmpty($user->getBlogposts());

        //test ajout de blogpost
        $user->addBlogpost($blogpost);
        $this->assertContains($blogpost, $user->getBlogposts());

        //test suppression de blogpost
        $user->removeBlogpost($blogpost);
        $this->assertEmpty($user->getBlogposts());
     }
}
