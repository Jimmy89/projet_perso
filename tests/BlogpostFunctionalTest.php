<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BlogpostFunctionalTest extends WebTestCase
{
    public function testShouldDisplayBlogpost()
    {
        $client = static::createClient();
        //URL de la page testé
        $crawler = $client->request('GET', '/actualites');

        $this->assertResponseIsSuccessful();
        //Vérification de la présence de la balise '' , puis de son contenu ''
        $this->assertSelectorTextContains('h2', 'Dernières Actualitées');
    }

    public function testShouldDisplayOneBlogpost()
    {
        $client = static::createClient();
        //URL de la page testé
        $crawler = $client->request('GET', '/actualites/blogpost-test');

        $this->assertResponseIsSuccessful();
        //Vérification de la présence de la balise '' , puis de son contenu ''
        $this->assertSelectorTextContains('h5', 'Blogpost test');
    }
}
