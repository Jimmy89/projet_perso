<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginFunctionalTest extends WebTestCase
{
    public function testShouldDisplayLogin()
    {
        $client = static::createClient();
        //URL de la page testé
        $crawler = $client->request('GET', '/login');

        $this->assertResponseIsSuccessful();
        //Vérification de la présence de la balise '' , puis de son contenu ''
        $this->assertSelectorTextContains('h1', 'Connexion');
    }

    public function testVisitingWhileLogin()
    {
        $client = static::createClient();
        //URL de la page testé
        $crawler = $client->request('GET', '/login');

        //recherche du boutton submit correspondant au formulaire voulu
        $buttonCrawlerNode = $crawler->selectButton('Connexion');
        //selection du formulaire qui correspond au bouton precedemment selectionner
        $form = $buttonCrawlerNode->form();

        //hydratation du formulaire avec les données tests
        $form = $buttonCrawlerNode->form([
            'email' => 'user@test.com',
            'password' => 'password',
        ]);

        //simulation de la validation du formulaire par le visiteur
        $client->submit($form);
        // simuler une redirection sur la page login
        $crawler = $client->request('GET', '/login');

        $this->assertResponseIsSuccessful();
        //Vérification de la présence de la balise '' , puis de son contenu ''
        $this->assertSelectorTextContains('h1', 'Vous êtes loggué en tant que user@test.com');
    }
}
