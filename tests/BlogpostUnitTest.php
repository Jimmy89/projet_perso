<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class BlogpostUnitTest extends TestCase
{
    public function testBlogpostIsTrue(): void
    {
        $blogpost = new Blogpost();
        $datetime = new DateTime();
        $user = new User();

        $blogpost->setTitre('Titre')
             ->setContenu('description')
             ->setCreatedAt($datetime)
             ->setSlug('slug')
             ->setUser($user);

        $this->assertTrue($blogpost->getTitre() === 'Titre');
        $this->assertTrue($blogpost->getContenu() === 'description');
        $this->assertTrue($blogpost->getSlug() === 'slug');
        $this->assertTrue($blogpost->getCreatedAt() === $datetime);
        $this->assertTrue($blogpost->getUser() === $user);
    }

    public function testBlogpostIsFalse(): void
    {
        $blogpost = new Blogpost();
        $datetime = new DateTime();
        $user = new User();

        $blogpost->setTitre('Titre')
             ->setContenu('description')
             ->setCreatedAt($datetime)
             ->setSlug('slug')
             ->setUser($user);

             $this->assertFalse($blogpost->getTitre() === 'false');
             $this->assertFalse($blogpost->getContenu() === 'false');
             $this->assertFalse($blogpost->getSlug() === 'false');
             $this->assertFalse($blogpost->getCreatedAt() === new DateTime());
             $this->assertFalse($blogpost->getUser() === new User());
    }

    public function testBlogpostIsEmpty(): void
    {
        $blogpost = new Blogpost();
        
        $this->assertEmpty($blogpost->getTitre());
             $this->assertEmpty($blogpost->getContenu());
             $this->assertEmpty($blogpost->getSlug());
             $this->assertEmpty($blogpost->getCreatedAt());
             $this->assertEmpty($blogpost->getId());
    }

     public function testAddGetRemoveCommentaire()
     {
        //initialisation du test 
        $blogpost = new Blogpost();
        $commentaire = new Commentaire();

        //test à vide
        $this->assertEmpty($blogpost->getCommentaires());

        //test ajout de commentaire
        $blogpost->addCommentaire($commentaire);
        $this->assertContains($commentaire, $blogpost->getCommentaires());

        //test suppression de commentaire
        $blogpost->removeCommentaire($commentaire);
        $this->assertEmpty($blogpost->getCommentaires());
     }
}
