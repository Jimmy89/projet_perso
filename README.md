# projet_perso
docker mysql phpmyadmin serveur-apache

# Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose
* nodejs et npm ou yarn
* php-xdebug (pour rapport de couverture de code)

Pour vérifier les pré-requis avec la Cli Symfony

```bash
symfony check:requirements

Pour demarrer un nouveau projet
symfony new my_project_directory --webapp

information pour git
git config --global user.email "xxx"
   && git config --global user.name "xxx"
```

### Lancer l'environnement de developement

ATTENTION pour reprendre cette archive il faut penser a modifier le nom projet_perso par le nom du projet dans les fichier vhosts, docker-compose.yml, .env et le nom du dossier du projet

```bash
composer install
npm install
npm run build

Pour lancer le projet il faut aller dans le dossier docker et lancer la commande 
docker-compose up -d --build

Pour acceder au terminal symfony
docker exec -it www_projet_perso bash
```
### jouer les fixtures

```bash
symfony console doctrine:fixtures:load
```

## Lancer les test

ATTENTION à la longueur des lignes la règle PSR12 mis en place dans la pipeline ce qui peut créer des erreurs.
ATTENTION cette archive arrive dans le var/www/html il faut retourner dans le var/www/ pour effectuer les commandes
ATTENTION il faut execute la commande chmod -R 777 public/ pour permettre l'écriture dans le dossier public

```bash
creer un test
symfony console make:test

TestUnitaire -> TestCase
TestFonctionel -> WebTestCase


detecter les erreurs de syntax
phpcs -v --standard=PSR12 --ignore=./src/Kernel.php ./src

jouer les tests unitaires
php bin/phpunit --testdox

voir le % de code couvert par les tests (le chemin est modifiable il envoie vers le dossier var du projet symfony)
pour le visualiser il faut ouvrir le fichier index.html dans le chemin indiqué
XDEBUG_MODE=coverage php bin/phpunit --coverage-html var/log/test/test-coverage

ATTENTION !!! pour les tests fonctionnel il faut aller dans le bash www et aller dans le chemin var/www pour taper la commande

creer une base de donnée pour les tests fonctionnel (si il y a une erreur Commentes "dbname_suffix: '_test%env(default::TEST_TOKEN)%'" dans le fichier de configuration config/packages/doctrine.yaml)
ATTENTION !!! pour les tests fonctionnels nous testons tout ce qui possede un slug !

APP_ENV=test symfony console doctrine:database:create
APP_ENV=test symfony console doctrine:migrations:migrate
APP_ENV=test symfony console doctrine:fixtures:load
```
#### Astuce

si on ajoute --dev a la fin des commande composer require le bundle ne s'installe que pour la partie dev
les fixtures sont à programmer dans le dossier src/DataFixtures

## Production

### Envoie des mails de Contacts

Les mails de prise de contact sont stocké en BDD, pour les envoyer au peintre par mail, il faut mettre en place un cron sur la commande dans le dossier src/command :

```bash
symfony console app:send-contact
```