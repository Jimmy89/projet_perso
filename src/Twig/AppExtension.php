<?php

namespace App\Twig;

use App\Repository\CategoryRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    // création des variables
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getFunctions(): array
    {
        return [
            // création de la fonction categoryHeader() a qui on donne la fonction category comme parametre
            new TwigFunction('categoryHeader', [$this, 'category']),
        ];
    }

    public function category(): array
    {
        // récuperation de toute les category
        return $this->categoryRepository->findAll();
    }
}
