<?php

namespace App\EventSubscriber;

use App\Entity\Blogpost;
use App\Entity\Peinture;
use DateTime;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    private $slugger;
    private $security;

    //permet a symfony de génerer un slug à partir d'une chaine de caractères
    public function __construct(SluggerInterface $slugger, Security $security)
    {
        $this->slugger = $slugger;
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        //Permet d'écouter la persistance des Entitées pour effectuer des actions
        //avant de les persister en base de données
        return [
            BeforeEntityPersistedEvent::class => ['setBlogPostSlugAndDateAndUser'],
        ];
    }

    //fonction Event à écouter
    public function setBlogPostSlugAndDateAndUser(BeforeEntityPersistedEvent $event)
    {
        //detai de l'event (dans l'exemple c'est un event d'entité BlogPost)
        $entity = $event->getEntityInstance();

        //on vérifie que c'est bien l'entité désiré que l'on écoute
        if (!($entity instanceof Blogpost || $entity instanceof Peinture)) {
            return;
        }

        // Methode1. On a fait une 2eme methode dans les fichiers CrudController dans le dossier src/admin
        // // double condition pour le slug
        // if ($entity instanceof Blogpost) {
        //     // on genere le slug à partir du titre
        //     $slug = $this->slugger->slug($entity->getTitre());
        //     $entity->setSlug($slug);
        // }

        // if ($entity instanceof Peinture) {
        //     // on genere le slug à partir du Nom
        //     $slug = $this->slugger->slug($entity->getNom());
        //     $entity->setSlug($slug);
        // }

        // on genere la date à partir de la date du jour
        $now = new DateTime('now');
        $entity->setCreatedAt($now);

        $user = $this->security->getUser();
        $entity->setUser($user);
    }
}
