<?php

namespace App\Repository;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Commentaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commentaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commentaire[]    findAll()
 * @method Commentaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commentaire::class);
    }

    //on passe en valeur à la fonction le blogpost ou la peintures
    public function findCommentaires($value)
    {
        // si la valeur fait partie de l'entité Blogpost alors on creer un objet blogpost
        if ($value instanceof Blogpost) {
            $object = '.blogpost';
        }

        // si la valeur fait partie de l'entité Peinture alors on creer un objet peinture
        if ($value instanceof Peinture) {
            $object = '.peinture';
        }

        // on met un alias a la table commentaire
        return $this->createQueryBuilder('c')
        // le champ $objet est égal à la variable valeur
        ->andWhere('c' . $object . ' = :val')
        // le commentaire doit être validé
        ->andWhere('c.isPublished = true')
        // la variable valeur donne l'Id de l'instance de la $value donc blogpost ou peinture
        ->SetParameter('val', $value->getId())
        // classer de manière décroissante
        ->orderBy('c.id', 'DESC')
        ->getQuery()
        ->getResult()
        ;
    }
}
