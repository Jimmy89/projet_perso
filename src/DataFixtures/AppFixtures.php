<?php

namespace App\DataFixtures;

use App\Entity\Blogpost;
use App\Entity\Category;
use App\Entity\Peinture;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker\Factory;

//Permet de demande a phpunit d'ignorer ce fichier
/**
 * @codeCoverageIgnore
 */
class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        // use the factory to create a Faker\Generator instance
        $faker = Factory::create('fr_FR');


            //Création d'un utilisateur
            $user = new User();
            $user->setEmail('user@test.com')
                ->setPrenom($faker->firstName())
                ->setNom($faker->lastName())
                ->setTelephone($faker->phoneNumber())
                ->setAPropos($faker->text())
                ->setRoles(['ROLE_PEINTRE'])
                ->setInstagram('instagram');
            // les fonctions sont dans la doc de phpfaker(https://github.com/FakerPHP/Faker)

            //mise en place du hashage de password
            $password = $this->encoder->encodePassword($user, 'password');
            $user->setPassword($password);

            // mise en tampon des données
            $manager->persist($user);

            // boucle 1
        for ($i = 0; $i < 10; $i++) {
            $blogpost = new Blogpost();

            $blogpost->setTitre($faker->words(3, true))
                    ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                    ->setContenu($faker->text(350))
                    ->setSlug($faker->slug(3))
                    ->setUser($user);

            // mise en tampon des données
            $manager->persist($blogpost);
        }

        // Création d'un BlogPost pour les tests
        $blogpost = new Blogpost();

            $blogpost->setTitre('Blogpost test')
                    ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                    ->setContenu($faker->text(350))
                    ->setSlug('blogpost-test')
                    ->setUser($user);

            // mise en tampon des données
            $manager->persist($blogpost);

        // boucle 2
        for ($j = 0; $j < 10; $j++) {
            $categorie = new Category();

            $categorie->setNom($faker->word())
                    ->setDescription($faker->words(10, true))
                    ->setSlug($faker->slug(3));

            // mise en tampon des données
            $manager->persist($categorie);

            //Sous boucle 1
            for ($k = 0; $k < 10; $k++) {
                $peinture = new Peinture();

                $peinture->setNom($faker->words(3, true))
                        ->setLargeur($faker->randomFloat(2, 20, 60))
                        ->setHauteur($faker->randomFloat(2, 20, 60))
                        ->setEnVente($faker->randomElement([true, false]))
                        ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                        ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
                        ->setDescription($faker->text())
                        ->setPortfolio($faker->randomElement([true, false]))
                        ->setSlug($faker->slug())
                        ->setFile('logo.png')
                        ->addCategory($categorie)
                        ->setPrix($faker->randomFloat(2, 100, 9999999))
                        ->setUser($user);

                // mise en tampon des données
                $manager->persist($peinture);
            }
        }

        //Categorie pour les tests
        $categorie = new Category();

            $categorie->setNom('categorie test')
                    ->setDescription($faker->words(10, true))
                    ->setSlug('categorie-test');

            // mise en tampon des données
            $manager->persist($categorie);

            //Peinture pour les tests
            $peinture = new Peinture();

            $peinture->setNom('Peinture test')
                    ->setLargeur($faker->randomFloat(2, 20, 60))
                    ->setHauteur($faker->randomFloat(2, 20, 60))
                    ->setEnVente($faker->randomElement([true, false]))
                    ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                    ->setDescription($faker->text())
                    ->setPortfolio($faker->randomElement([true, false]))
                    ->setSlug('peinture-test')
                    ->setFile('logo.png')
                    ->addCategory($categorie)
                    ->setPrix($faker->randomFloat(2, 100, 9999999))
                    ->setUser($user);

            // mise en tampon des données
            $manager->persist($peinture);


        // enregistrement en base
        $manager->flush();
    }
}
