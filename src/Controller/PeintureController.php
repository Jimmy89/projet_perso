<?php

namespace App\Controller;

use App\Entity\Commentaire;
use App\Entity\Peinture;
use App\Form\CommentaireType;
use App\Repository\CommentaireRepository;
use App\Repository\PeintureRepository;
use App\Services\CommentaireService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

    /**
     * @Route("/realisations")
     */

class PeintureController extends AbstractController
{
    /**
     * @Route("", name="app_realisations")
     */
    public function realisations(
        PeintureRepository $peintureRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        // récuperation de toute les peintures par ordre decroissant
        $data = $peintureRepository->findBy([], ['id' => 'DESC']);

        // récuperation des peintures sur la page actuelle paginate(quelle données,
        //a quel page en est t'il et comment les délimités,limite d'element)
        $peintures = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('peinture/realisations.html.twig', [
            'peintures' => $peintures,
        ]);
    }

    /**
     * @Route("/{slug}", name="realisations_details")
     */
    public function details(
        Peinture $peinture,
        Request $request,
        CommentaireService $commentaireService,
        CommentaireRepository $commentaireRepository
    ): Response {

            $commentaires = $commentaireRepository->findCommentaires($peinture);
            $commentaire = new Commentaire();
            $form = $this->createForm(CommentaireType::class, $commentaire);
            $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
                // on demande si le formulaire contient de la data
                $commentaire = $form->getData();

                // Recuperation du service commentaireService appeler dans la fonction
                // dans lequel on appel la fonction persistcommentaire qui nous demande un commentaire en objet
                // un blogpost et une peinture que l'on met à null
                $commentaireService->persistCommentaire($commentaire, null, $peinture);

                return $this->redirectToRoute('realisations_details', ['slug' => $peinture->getSlug()]);
                // die & dump pour le formulaire
                //dd($commentaire);
        }

        return $this->render('peinture/details.html.twig', [
            'peinture' => $peinture,
            'form' => $form->createView(),
            'commentaires' => $commentaires,
        ]);
    }
}
