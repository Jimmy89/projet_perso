<?php

namespace App\Controller;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Form\CommentaireType;
use App\Repository\BlogpostRepository;
use App\Repository\CommentaireRepository;
use App\Services\CommentaireService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

    /**
     * @Route("/actualites")
     */

class BlogpostController extends AbstractController
{
    /**
     * @Route("", name="app_actualites")
     */
    public function actualites(
        BlogpostRepository $blogpostRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        // récuperation de tout les blogposts
        $data = $blogpostRepository->findBy([], ['id' => 'DESC']);

        // récuperation des blogposts sur la page actuelle paginate(quelle données,
        //a quel page en est t'il et comment les délimités,limite d'element)
        $blogposts = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('blogpost/actualites.html.twig', [
            'blogposts' => $blogposts,
        ]);
    }

    /**
     * @Route("/{slug}", name="blogposts_details")
     */
    public function details(
        Blogpost $blogpost,
        Request $request,
        CommentaireService $commentaireService,
        CommentaireRepository $commentaireRepository
    ): Response {

            $commentaires = $commentaireRepository->findCommentaires($blogpost);
            $commentaire = new Commentaire();
            $form = $this->createForm(CommentaireType::class, $commentaire);
            $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
                // on demande si le formulaire contient de la data
                $commentaire = $form->getData();

                // Recuperation du service commentaireService appeler dans la fonction
                // dans lequel on appel la fonction persistcommentaire qui nous demande un commentaire en objet
                // un blogpost et une peinture que l'on met à null
                $commentaireService->persistCommentaire($commentaire, $blogpost, null);

                return $this->redirectToRoute('blogposts_details', ['slug' => $blogpost->getSlug()]);
                // die & dump pour le formulaire
                //dd($commentaire);
        }

        return $this->render('blogpost/details.html.twig', [
            'blogpost' => $blogpost,
            'form' => $form->createView(),
            'commentaires' => $commentaires,
        ]);
    }
}
