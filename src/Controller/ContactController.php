<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Services\ContactService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="app_contact")
     */
    public function index(Request $request, ContactService $contactService): Response
    {
        $contact = new Contact();

        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        // vérifie que le formulaire est soumis et valide
        if ($form->isSubmitted() && $form->isValid()) {
            // on demande si le formulaire contient de la data
            $contact = $form->getData();

            // Recuperation du service ContactService appeler dans la fonction
            // dans lequel on appel la fonction persistContact qui nous demande un contact en objet
            $contactService->persistContact($contact);

            return $this->redirectToRoute('app_contact');
            // die & dump pour le formulaire
            //dd($contact);
        }

        return $this->render('contact/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
