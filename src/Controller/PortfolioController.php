<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Repository\PeintureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/portfolio")
 */

class PortfolioController extends AbstractController
{
    /**
     * @Route("", name="app_portfolio")
     */
    public function index(CategoryRepository $categoryRepository): Response
    {
        return $this->render('portfolio/index.html.twig', [
            'categories' => $categoryRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{slug}", name="portfolio_categorie")
     */
    public function categorie(Category $category, PeintureRepository $peintureRepository): Response
    {
        $peintures = $peintureRepository->findAllPortfolio($category);

        return $this->render('portfolio/categorie.html.twig', [
            'categorie' => $category,
            'peintures' => $peintures,
        ]);
    }
}
