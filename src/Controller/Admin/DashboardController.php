<?php

namespace App\Controller\Admin;

use App\Entity\Blogpost;
use App\Entity\Category;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    //l'accès restreint à l'admin ce gère dans le config/security.yaml rubrique access_control:
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        // remplacement de la page d'accueil par default
        // return parent::index();
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Projet Perso');
    }

    public function configureMenuItems(): iterable
    {
        // Element du menu voir doc pour les différent type d'item
        // https://symfony.com/bundles/EasyAdminBundle/current/dashboards.html#menu-item-configuration-options
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Actualités', 'fas fa-newspaper', Blogpost::class);
        yield MenuItem::linkToCrud('Réalisation', 'fas fa-palette', Peinture::class);
        yield MenuItem::linkToCrud('Paramètres', 'fas fa-cog', User::class);
        yield MenuItem::linkToCrud('Commentaire', 'fas fa-comment', Commentaire::class);
        yield MenuItem::linkToCrud('Catégorie', 'fas fa-tags', Category::class);
        yield MenuItem::linkToRoute('Retour sur le site', 'fas fa-list', 'app_home');
    }
}
