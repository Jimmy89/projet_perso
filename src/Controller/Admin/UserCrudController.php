<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    // permet de lister les champs dans le index
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nom'),
            TextField::new('prenom'),
            TextField::new('telephone'),
            TextField::new('instagram'),
            TextareaField::new('aPropos'),
        ];
    }

    // permet de configurer les actions possible (voir la doc easyadmin)
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // ajout d'un boutons qui permet d'acceder au détail
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            // désactiver l'option DELETE et l'option NEW
            ->disable(Action::DELETE, Action::NEW);
    }

    // Modifier les élements de la page (voir la doc easyadmin)
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'Paramètres')
            ->setPageTitle('edit', 'Editer les paramètres');
    }
}
