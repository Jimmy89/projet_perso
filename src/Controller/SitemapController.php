<?php

namespace App\Controller;

use App\Repository\BlogpostRepository;
use App\Repository\CategoryRepository;
use App\Repository\PeintureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    /**
     * @Route("/sitemap.xml", name="app_sitemap", defaults={"_format"="xml"})
     */
    public function index(
        Request $request,
        PeintureRepository $peintureRepository,
        BlogpostRepository $blogpostRepository,
        CategoryRepository $categoryRepository
    ): Response {
        // documentation sur le site sitemaps.org
        // on recupere le nom de domaine du site
        $hotsname = $request->getSchemeAndHttpHost();

        // on creer un tableau pour recuperer toute les URL du site
        $urls = [];
        // on utilise comme clé le nom des balises de la documentation et la valeur est le nom de la route
        $urls[] = ['loc' => $this->generateUrl('app_home')];
        $urls[] = ['loc' => $this->generateUrl('app_realisations')];
        $urls[] = ['loc' => $this->generateUrl('app_actualites')];
        $urls[] = ['loc' => $this->generateUrl('app_portfolio')];
        $urls[] = ['loc' => $this->generateUrl('app_a_propos')];
        $urls[] = ['loc' => $this->generateUrl('app_contact')];

        // on boucle les peintures, ... pour recuperer toute les urls
        foreach ($peintureRepository->findAll() as $peinture) {
            $urls[] = [
                'loc' => $this->generateUrl('realisations_details', ['slug' => $peinture->getSlug()]),
                'lastmod' => $peinture->getCreatedAt()->format('Y-m-d')
            ];
        }

        foreach ($blogpostRepository->findAll() as $blogpost) {
            $urls[] = [
                'loc' => $this->generateUrl('blogposts_details', ['slug' => $blogpost->getSlug()]),
                'lastmod' => $blogpost->getCreatedAt()->format('Y-m-d')
            ];
        }

        foreach ($categoryRepository->findAll() as $category) {
            $urls[] = [
                'loc' => $this->generateUrl('portfolio_categorie', ['slug' => $category->getSlug()])
            ];
        }

        // création de la réponse car le render habituel ne fonctionnera pas
        $response = new Response(
            $this->renderView('sitemap/index.html.twig', [
                'urls' => $urls,
                'hostname' => $hotsname,
            ]),
            200
        );

        // modification de l'en-têtede la response
        $response->headers->set('Content-type', 'text/xml');

        // renvoi au robot la response
        return $response;

        // la suite ce passe dans la vue sitemap/index.html.twig
    }
}
