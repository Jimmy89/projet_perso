<?php

namespace App\Services;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class CommentaireService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistCommentaire(
        Commentaire $commentaire,
        Blogpost $blogpost = null,
        Peinture $peinture = null
    ): void {
        $commentaire->setIsPublished(false)
                ->setBlogpost($blogpost)
                ->setPeinture($peinture)
                ->setCreatedAt(new DateTime('now'));

        $this->manager->persist($commentaire);
        $this->manager->flush();
        //permet d'envoyer un message à la vue. ATTENTION il faut le FlashBagInterface pour le générer
        $this->flash->add('success', 'Votre commentaire est bien envoyé, merci. Il sera publié après validation');
    }
}
