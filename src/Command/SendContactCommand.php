<?php

// fichier de commande pour envoie de mail en différé

namespace App\Command;

use App\Repository\ContactRepository;
use App\Repository\UserRepository;
use App\Services\ContactService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class SendContactCommand extends Command
{
    //recuperation de la liste des mails
    private $contactRepository;
    // fonction mail
    private $mailer;
    // recuperation du service pour mettre les mails en envoyé = true
    private $contactService;
    // recuperation de l'adresse mail de l'utilisateur peintre
    private $userRepository;
    // nom de la commande pour l'exemple il faudra taper (symfony console app:send-contact) pour l'executer
    protected static $defaultName = 'app:send-contact';

    public function __construct(
        ContactRepository $contactRepository,
        MailerInterface $mailer,
        ContactService $contactService,
        UserRepository $userRepository
    ) {
        $this->contactRepository = $contactRepository;
        $this->mailer = $mailer;
        $this->contactService = $contactService;
        $this->userRepository = $userRepository;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //recuperation de la liste des mails qui ne sont pas encore envoyé
        $toSend = $this->contactRepository->findBy(['isSend' => false]);
        // recuperation de l'adresse mail de l'utilisateur peintre
        $address = new Address(
            $this->userRepository->getPeintre()->getEmail(),
            $this->userRepository->getPeintre()->getNom() . ' ' . $this->userRepository->getPeintre()->getPrenom()
        );

        //boucle sur tout les mails que l'on stock dans la variable $mail
        foreach ($toSend as $mail) {
            $email = (new Email())
                //récupération de l'adresse mail saisie dans le formulaire
                ->from($mail->getEmail())
                ->to($address)
                //->cc('cc@example.com')
                //->bcc('bcc@example.com')
                //->replyTo('fabien@example.com')
                //->priority(Email::PRIORITY_HIGH)
                ->subject('Nouveau message de ' . $mail->getNom())
                ->text($mail->getMessage());
                //->html('<p>See Twig integration for better HTML integration!</p>');

            //envoi de mail
            $this->mailer->send($email);
            //changement du booleen a true des mails
            $this->contactService->isSend($mail);
        }

        //revoie de la valeur success de la commande
        return Command::SUCCESS;
    }
}
